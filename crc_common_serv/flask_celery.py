'''Utility functions for Celery tasks.'''

from celery import Celery


def make_celery(app):
    '''
    Flask recommended method to integrate Celery.
    
    http://flask.pocoo.org/docs/0.12/patterns/celery/
    
    Args:
        app (flask.Flask): The Flask application object.
    
    Returns:
        A celery object configured to work with Flask.
    '''

    celery = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery
