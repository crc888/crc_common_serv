'''This module is used to help manage JWT tokens.'''

from datetime import datetime, timedelta

from flask import current_app as app
from jwt import decode, encode, ExpiredSignatureError, InvalidTokenError


def encode_jwt(email, roles):
    '''
    Generate a JWT auth token for a user
    
    Args:
        email (str): The user's email.
        roles (:obj:`list` of :obj:`str`): A list of the user's role.
    
    Returns:
        The token if it was generated successfully, otherwise None.
    '''

    try:
        payload = {
            'exp': datetime.utcnow() + timedelta(minutes=app.config['JWT_TOKEN_EXP_MIN']),
            'iat': datetime.utcnow(),
            'sub': email,
            'roles': roles
        }
        return encode(
            payload,
            app.config['SECRET_KEY'],
            algorithm='HS256'
        )
    except Exception:
        return None


def validate_jwt(auth_bearer):
    '''
    Validate a JWT auth token for a user.
    
    Args:
        auth_bearer: Token string passed in the Authorization Bearer header.
    
    Returns:
        The token data as a python dictionary. The token itself is found at 
        the key "decoded". A boolean defining if the token is valid is found
        at the key "valid". If the token is invalid or expired, the key of 
        "ret_code" will contain the HTTP return code that should be sent
        back to the requester.
    '''

    # Initialize a dictionary to store the info about the token
    token_data = {}

    # Try to decode the token
    try:
        token_data['decoded'] = decode(auth_bearer, app.config['SECRET_KEY'])
        token_data['valid'] = True
    except ExpiredSignatureError:
        # The token has already expired
        token_data['resp'] = dict(
            status='failure',
            message='The token provided has expired'
        )
        token_data['ret_code'] = 401
        token_data['valid'] = False
    except InvalidTokenError:
        # The token is not valid
        token_data['resp'] = dict(
            status='failure',
            message='The token provided is invalid'
        )
        token_data['ret_code'] = 401
        token_data['valid'] = False

    return token_data
