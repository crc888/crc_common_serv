'''
Service specific functions to create a Flask app via the application factory 
pattern.
'''

from time import time

from flask import jsonify, request

from crc_common.custom_except import InvalidUsage


def configure_hooks_serv(app, stats):
    '''
    Configure the before and after request hooks for service apps.
    
    Args:
        app (flask.Flask): The Flask application object.
        stats: The Flask StatsD extension object.
    '''

    @app.before_request
    def before_request():
        '''
        Performed before every web request to start timing the request 
        duration.
        '''

        # Time every request
        request.start_time = time()

    @app.after_request
    def after_request(response):
        '''
        Store prometheus metrics after every request.
        
        Returns:
            The Flask response object.
        '''

        latency = time() - request.start_time

        # Increment the endpoint hits by return code
        stats.incr(
            app.config['APP_NAME'].replace(
                '-', '_') + '_' + str(request.endpoint) + '_' + str(response.status_code)
        )

        # Time the latency for the endpoint
        stats.timing(
            app.config['APP_NAME'].replace(
                '-', '_') + '_' + str(request.endpoint) + '_latency_sec', latency
        )

        # Return the response unaltered
        return response


def set_error_handlers_serv(app):
    '''
    Configure service error handlers.
    
    Args:
        app (flask.Flask): The Flask application object.
    '''

    @app.errorhandler(500)
    def internal_error(error):
        '''
        Global error handler for uncaught exceptions. These errors are only
        logged. They are logged as a critical error.
        '''

        app.logger.critical(
            '500 Internal Server Error : {err:s}'.format(err=error.description)
        )

    @app.errorhandler(InvalidUsage)
    def custom_error(error):
        '''
        Global error handler custom "InvalidUsage" exceptions. These errors 
        are logged as a warning.
        
        Returns:
            The Flask response object.
        '''

        # Log the invalid usage error
        app.logger.warning(error.to_dict())

        # Send a response to the client indicating why the request is invalid
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response
