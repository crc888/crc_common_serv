'''Unit tests for the token module.'''

import unittest

from flask import Flask

from crc_common_serv.token import encode_jwt, validate_jwt

# Global test data
EMAIL = 'mail@mail.com'
FAIL_MSG = 'failure'
INVALID_MSG = 'The token provided is invalid'


class TestToken(unittest.TestCase):
    '''Class containing all tests for the token module.'''

    def setUp(self):
        '''
        Create a Flask object for all tests. Set the Flask config variables as
        needed for testing as well.
        '''
        
        self.app = Flask(__name__)
        self.app.config['JWT_TOKEN_EXP_MIN'] = 1
        self.app.config['SECRET_KEY'] = 'its a secret to everyone'

    def test_jwt_pass(self):
        '''Ensure that valid tokens are validated properly.'''

        # Worked inside of the Flask application context
        with self.app.app_context():
            
            # Call the encode_jwt function
            auth_bearer_token = encode_jwt(EMAIL, [])
    
            # Call the validate_jwt function
            token_data = validate_jwt(auth_bearer_token)
            
            # Validate the token_data
            assert token_data['valid'] == True
            assert token_data['decoded']['sub'] == EMAIL

    def test_jwt_invalid(self):
        '''Ensure that invalid tokens are rejected.'''

        # Worked inside of the Flask application context
        with self.app.app_context():

            # Call the validate_jwt function
            token_data = validate_jwt('bad token')
            
            # Validate the token_data
            assert token_data['valid'] == False
            assert token_data['resp']['status'] == FAIL_MSG
            assert token_data['resp']['message'] == INVALID_MSG
