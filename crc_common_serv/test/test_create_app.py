'''Unit tests for the create_app module.'''

import unittest

from flask import Flask

from crc_common.custom_except import InvalidUsage
from crc_common_serv.create_app import set_error_handlers_serv

# Global test data
LOGIN_MSG = 'Please log in to access that page'
USER_ID = '1'


class TestCreateApp(unittest.TestCase):
    '''Class containing all tests for the create_app function.'''

    def setUp(self):
        self.app = Flask(__name__)

    def test_error_handlers(self):
        '''Validate that the app has the desired error handlers'''

        # Call the set_error_handlers_fe function
        set_error_handlers_serv(self.app)

        # Validate Standard HTTP error handlers
        assert 500 in self.app.error_handlers

        # Validate custom InvalidUsage error handler
        err = InvalidUsage(api='', message='', status='')
        assert self.app.error_handlers[None][type(err)] is not None
