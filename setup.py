'''Install the crc_common_serv package'''

from setuptools import setup

setup(
    name='crc_common_serv',
    version='0.3',
    description='Common modules for CRC back end services',
    author='Craig Ciccone',
    author_email='crc888@gmail.com',
    url='https://www.crc-web.cf/',
    packages=['crc_common_serv'],
    dependency_links=[
        'https://bitbucket.org/crc888/crc_common/get/master.zip#egg=crc-common-0.2'
    ],
    install_requires=[
        'crc-common==0.3',
        'celery==4.1.0',
        'redis==2.10.6',
        'PyJWT==1.5.3'
    ]
)
